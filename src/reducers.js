import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import localeReducer, { storedKey as storedLocaleState } from '@containers/Locale/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  locale: { reducer: localeReducer, whitelist: storedLocaleState },
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
