import { SET_THEME } from '@containers/Locale/constants';

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});
