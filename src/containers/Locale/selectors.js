import { createSelector } from 'reselect';
import { initialState } from '@containers/Locale/reducer';

const selectLocaleState = (state) => state.locale || initialState;

const selectTheme = createSelector(selectLocaleState, (state) => state.theme);

export { selectTheme };
