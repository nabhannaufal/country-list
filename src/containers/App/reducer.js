import { produce } from 'immer';

import {
  SET_COUNTRY_LIST,
  SET_COUNTRY_LOADING,
  SET_COUNTRY_ERROR,
  SET_COUNTRY_DETAIL,
  SET_COUNTRY_DETAIL_ERROR,
} from '@containers/App/constants';

export const initialState = {
  countryList: [],
  countryLoading: false,
  countryError: null,
  countryDetail: {},
  countryDetailError: null,
};

// eslint-disable-next-line default-param-last
const appReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_COUNTRY_LIST:
        draft.countryList = action.countryList;
        break;
      case SET_COUNTRY_LOADING:
        draft.countryLoading = action.countryLoading;
        break;
      case SET_COUNTRY_ERROR:
        draft.countryError = action.countryError;
        break;
      case SET_COUNTRY_DETAIL:
        draft.countryDetail = action.countryDetail;
        break;
      case SET_COUNTRY_DETAIL_ERROR:
        draft.countryDetailError = action.countryDetailError;
        break;
      default:
        break;
    }
  });

export default appReducer;
