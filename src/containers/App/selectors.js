import { createSelector } from 'reselect';
import { initialState } from '@containers/App/reducer';

const selectAppState = (state) => state.app || initialState;

const selectCountryList = createSelector(selectAppState, (state) => state.countryList);
const selectCountryLoading = createSelector(selectAppState, (state) => state.countryLoading);
const selectCountryError = createSelector(selectAppState, (state) => state.countryError);
const selectCountryDetail = createSelector(selectAppState, (state) => state.countryDetail);
const selectCountryDetailError = createSelector(selectAppState, (state) => state.selectCountryDetailError);

export { selectCountryList, selectCountryLoading, selectCountryError, selectCountryDetail, selectCountryDetailError };
