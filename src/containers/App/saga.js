import { takeLatest, call, put } from 'redux-saga/effects';

import { GET_COUNTRY_DETAIL, GET_COUNTRY_LIST } from '@containers/App/constants';
import {
  setCountryList,
  setCountryLoading,
  setCountryError,
  setCountryDetail,
  setCountryDetailError,
} from '@containers/App//actions';
import { getCountryList, getCountryDetail } from '@domain/api';

export function* doGetCountryList({ region }) {
  yield put(setCountryLoading(true));
  yield put(setCountryError(null));
  try {
    const countryList = yield call(getCountryList, region);
    if (countryList) {
      yield put(setCountryList(countryList));
      yield put(setCountryError(null));
    }
  } catch (error) {
    yield put(setCountryError(error));
  }
  yield put(setCountryLoading(false));
}

export function* doGetCountryDetail({ name }) {
  yield put(setCountryLoading(true));
  yield put(setCountryError(null));
  try {
    const countryDetail = yield call(getCountryDetail, name);
    if (countryDetail && countryDetail.length > 0) {
      yield put(setCountryDetail(countryDetail[0]));
      yield put(setCountryError(null));
    }
  } catch (error) {
    yield put(setCountryDetailError(error));
  }
  yield put(setCountryLoading(false));
}

export default function* appSaga() {
  yield takeLatest(GET_COUNTRY_LIST, doGetCountryList);
  yield takeLatest(GET_COUNTRY_DETAIL, doGetCountryDetail);
}
