import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectTheme } from '@containers/Locale/selectors';

import ClientRoutes from '@routes/ClientRoutes';
import Theme from '@containers/Theme';

const App = ({ theme }) => (
  <Theme theme={theme}>
    <ClientRoutes />
  </Theme>
);

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

App.propTypes = {
  theme: PropTypes.string,
};

export default connect(mapStateToProps)(App);
