import {
  GET_COUNTRY_DETAIL,
  GET_COUNTRY_LIST,
  SET_COUNTRY_DETAIL,
  SET_COUNTRY_DETAIL_ERROR,
  SET_COUNTRY_ERROR,
  SET_COUNTRY_LIST,
  SET_COUNTRY_LOADING,
} from '@containers/App/constants';

export const getCountryList = (region) => ({
  type: GET_COUNTRY_LIST,
  region,
});

export const setCountryList = (countryList) => ({
  type: SET_COUNTRY_LIST,
  countryList,
});

export const setCountryLoading = (countryLoading) => ({
  type: SET_COUNTRY_LOADING,
  countryLoading,
});

export const setCountryError = (countryError) => ({
  type: SET_COUNTRY_ERROR,
  countryError,
});

export const getCountryDetail = (name) => ({
  type: GET_COUNTRY_DETAIL,
  name,
});

export const setCountryDetail = (countryDetail) => ({
  type: SET_COUNTRY_DETAIL,
  countryDetail,
});

export const setCountryDetailError = (countryDetailError) => ({
  type: SET_COUNTRY_DETAIL_ERROR,
  countryDetailError,
});
