import axios from 'axios';

const urls = {
  countryList: 'https://restcountries.com/v3.1',
};

export const callAPI = async (endpoint, method, headers = {}, params = {}, data = {}) => {
  const options = {
    url: endpoint,
    method,
    headers,
    data,
    params,
  };

  return axios(options).then((response) => {
    const responseAPI = response.data;
    return responseAPI;
  });
};

export const getCountryList = (region) => {
  const prefix = region ? `/region/${region}` : '/all';
  return callAPI(urls.countryList + prefix, 'get');
};

export const getCountryDetail = (name) => {
  const prefix = `/name/${name}`;
  return callAPI(urls.countryList + prefix, 'get');
};
