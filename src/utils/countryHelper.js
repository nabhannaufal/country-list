export const codeList = [
  {
    code: 'ASM',
    name: 'American Samoa',
  },
  {
    code: 'PER',
    name: 'Peru',
  },
  {
    code: 'TON',
    name: 'Tonga',
  },
  {
    code: 'MYT',
    name: 'Mayotte',
  },
  {
    code: 'PAN',
    name: 'Panama',
  },
  {
    code: 'MDG',
    name: 'Madagascar',
  },
  {
    code: 'BEL',
    name: 'Belgium',
  },
  {
    code: 'CAF',
    name: 'Central African Republic',
  },
  {
    code: 'HUN',
    name: 'Hungary',
  },
  {
    code: 'MNG',
    name: 'Mongolia',
  },
  {
    code: 'SAU',
    name: 'Saudi Arabia',
  },
  {
    code: 'SWZ',
    name: 'Eswatini',
  },
  {
    code: 'UGA',
    name: 'Uganda',
  },
  {
    code: 'PRY',
    name: 'Paraguay',
  },
  {
    code: 'CMR',
    name: 'Cameroon',
  },
  {
    code: 'FRA',
    name: 'France',
  },
  {
    code: 'TGO',
    name: 'Togo',
  },
  {
    code: 'MUS',
    name: 'Mauritius',
  },
  {
    code: 'COK',
    name: 'Cook Islands',
  },
  {
    code: 'PLW',
    name: 'Palau',
  },
  {
    code: 'NPL',
    name: 'Nepal',
  },
  {
    code: 'NZL',
    name: 'New Zealand',
  },
  {
    code: 'PCN',
    name: 'Pitcairn Islands',
  },
  {
    code: 'SYC',
    name: 'Seychelles',
  },
  {
    code: 'ARE',
    name: 'United Arab Emirates',
  },
  {
    code: 'BLR',
    name: 'Belarus',
  },
  {
    code: 'DOM',
    name: 'Dominican Republic',
  },
  {
    code: 'LBY',
    name: 'Libya',
  },
  {
    code: 'TKL',
    name: 'Tokelau',
  },
  {
    code: 'HRV',
    name: 'Croatia',
  },
  {
    code: 'PHL',
    name: 'Philippines',
  },
  {
    code: 'ALB',
    name: 'Albania',
  },
  {
    code: 'BVT',
    name: 'Bouvet Island',
  },
  {
    code: 'IDN',
    name: 'Indonesia',
  },
  {
    code: 'THA',
    name: 'Thailand',
  },
  {
    code: 'LVA',
    name: 'Latvia',
  },
  {
    code: 'ECU',
    name: 'Ecuador',
  },
  {
    code: 'KEN',
    name: 'Kenya',
  },
  {
    code: 'LBR',
    name: 'Liberia',
  },
  {
    code: 'VGB',
    name: 'British Virgin Islands',
  },
  {
    code: 'SMR',
    name: 'San Marino',
  },
  {
    code: 'IND',
    name: 'India',
  },
  {
    code: 'BHR',
    name: 'Bahrain',
  },
  {
    code: 'MWI',
    name: 'Malawi',
  },
  {
    code: 'ISL',
    name: 'Iceland',
  },
  {
    code: 'BRN',
    name: 'Brunei',
  },
  {
    code: 'GUF',
    name: 'French Guiana',
  },
  {
    code: 'WSM',
    name: 'Samoa',
  },
  {
    code: 'GEO',
    name: 'Georgia',
  },
  {
    code: 'BES',
    name: 'Caribbean Netherlands',
  },
  {
    code: 'HMD',
    name: 'Heard Island and McDonald Islands',
  },
  {
    code: 'PNG',
    name: 'Papua New Guinea',
  },
  {
    code: 'GHA',
    name: 'Ghana',
  },
  {
    code: 'AFG',
    name: 'Afghanistan',
  },
  {
    code: 'CRI',
    name: 'Costa Rica',
  },
  {
    code: 'FJI',
    name: 'Fiji',
  },
  {
    code: 'KOR',
    name: 'South Korea',
  },
  {
    code: 'TWN',
    name: 'Taiwan',
  },
  {
    code: 'CZE',
    name: 'Czechia',
  },
  {
    code: 'NIC',
    name: 'Nicaragua',
  },
  {
    code: 'PRT',
    name: 'Portugal',
  },
  {
    code: 'CHL',
    name: 'Chile',
  },
  {
    code: 'GMB',
    name: 'Gambia',
  },
  {
    code: 'BLZ',
    name: 'Belize',
  },
  {
    code: 'MDV',
    name: 'Maldives',
  },
  {
    code: 'VUT',
    name: 'Vanuatu',
  },
  {
    code: 'MYS',
    name: 'Malaysia',
  },
  {
    code: 'ATG',
    name: 'Antigua and Barbuda',
  },
  {
    code: 'IRN',
    name: 'Iran',
  },
  {
    code: 'RUS',
    name: 'Russia',
  },
  {
    code: 'KIR',
    name: 'Kiribati',
  },
  {
    code: 'MTQ',
    name: 'Martinique',
  },
  {
    code: 'BGD',
    name: 'Bangladesh',
  },
  {
    code: 'ARM',
    name: 'Armenia',
  },
  {
    code: 'MEX',
    name: 'Mexico',
  },
  {
    code: 'EST',
    name: 'Estonia',
  },
  {
    code: 'OMN',
    name: 'Oman',
  },
  {
    code: 'USA',
    name: 'United States',
  },
  {
    code: 'LIE',
    name: 'Liechtenstein',
  },
  {
    code: 'BHS',
    name: 'Bahamas',
  },
  {
    code: 'BEN',
    name: 'Benin',
  },
  {
    code: 'UNK',
    name: 'Kosovo',
  },
  {
    code: 'TUV',
    name: 'Tuvalu',
  },
  {
    code: 'FRO',
    name: 'Faroe Islands',
  },
  {
    code: 'CYM',
    name: 'Cayman Islands',
  },
  {
    code: 'EGY',
    name: 'Egypt',
  },
  {
    code: 'AZE',
    name: 'Azerbaijan',
  },
  {
    code: 'AUS',
    name: 'Australia',
  },
  {
    code: 'IMN',
    name: 'Isle of Man',
  },
  {
    code: 'IRL',
    name: 'Ireland',
  },
  {
    code: 'BRB',
    name: 'Barbados',
  },
  {
    code: 'SLV',
    name: 'El Salvador',
  },
  {
    code: 'GTM',
    name: 'Guatemala',
  },
  {
    code: 'JPN',
    name: 'Japan',
  },
  {
    code: 'VNM',
    name: 'Vietnam',
  },
  {
    code: 'MNP',
    name: 'Northern Mariana Islands',
  },
  {
    code: 'LTU',
    name: 'Lithuania',
  },
  {
    code: 'LCA',
    name: 'Saint Lucia',
  },
  {
    code: 'HKG',
    name: 'Hong Kong',
  },
  {
    code: 'AGO',
    name: 'Angola',
  },
  {
    code: 'MOZ',
    name: 'Mozambique',
  },
  {
    code: 'BWA',
    name: 'Botswana',
  },
  {
    code: 'DEU',
    name: 'Germany',
  },
  {
    code: 'SYR',
    name: 'Syria',
  },
  {
    code: 'MDA',
    name: 'Moldova',
  },
  {
    code: 'SPM',
    name: 'Saint Pierre and Miquelon',
  },
  {
    code: 'GGY',
    name: 'Guernsey',
  },
  {
    code: 'WLF',
    name: 'Wallis and Futuna',
  },
  {
    code: 'POL',
    name: 'Poland',
  },
  {
    code: 'TTO',
    name: 'Trinidad and Tobago',
  },
  {
    code: 'IOT',
    name: 'British Indian Ocean Territory',
  },
  {
    code: 'MHL',
    name: 'Marshall Islands',
  },
  {
    code: 'AUT',
    name: 'Austria',
  },
  {
    code: 'KAZ',
    name: 'Kazakhstan',
  },
  {
    code: 'ABW',
    name: 'Aruba',
  },
  {
    code: 'ZAF',
    name: 'South Africa',
  },
  {
    code: 'DNK',
    name: 'Denmark',
  },
  {
    code: 'SUR',
    name: 'Suriname',
  },
  {
    code: 'BMU',
    name: 'Bermuda',
  },
  {
    code: 'MSR',
    name: 'Montserrat',
  },
  {
    code: 'CIV',
    name: 'Ivory Coast',
  },
  {
    code: 'MRT',
    name: 'Mauritania',
  },
  {
    code: 'SLE',
    name: 'Sierra Leone',
  },
  {
    code: 'GIN',
    name: 'Guinea',
  },
  {
    code: 'PYF',
    name: 'French Polynesia',
  },
  {
    code: 'PAK',
    name: 'Pakistan',
  },
  {
    code: 'UMI',
    name: 'United States Minor Outlying Islands',
  },
  {
    code: 'LSO',
    name: 'Lesotho',
  },
  {
    code: 'SEN',
    name: 'Senegal',
  },
  {
    code: 'TJK',
    name: 'Tajikistan',
  },
  {
    code: 'CHE',
    name: 'Switzerland',
  },
  {
    code: 'ERI',
    name: 'Eritrea',
  },
  {
    code: 'ATF',
    name: 'French Southern and Antarctic Lands',
  },
  {
    code: 'JEY',
    name: 'Jersey',
  },
  {
    code: 'KHM',
    name: 'Cambodia',
  },
  {
    code: 'VCT',
    name: 'Saint Vincent and the Grenadines',
  },
  {
    code: 'SXM',
    name: 'Sint Maarten',
  },
  {
    code: 'VAT',
    name: 'Vatican City',
  },
  {
    code: 'URY',
    name: 'Uruguay',
  },
  {
    code: 'ESH',
    name: 'Western Sahara',
  },
  {
    code: 'CPV',
    name: 'Cape Verde',
  },
  {
    code: 'ITA',
    name: 'Italy',
  },
  {
    code: 'GBR',
    name: 'United Kingdom',
  },
  {
    code: 'MAC',
    name: 'Macau',
  },
  {
    code: 'GUY',
    name: 'Guyana',
  },
  {
    code: 'NOR',
    name: 'Norway',
  },
  {
    code: 'NCL',
    name: 'New Caledonia',
  },
  {
    code: 'SSD',
    name: 'South Sudan',
  },
  {
    code: 'BFA',
    name: 'Burkina Faso',
  },
  {
    code: 'GLP',
    name: 'Guadeloupe',
  },
  {
    code: 'LBN',
    name: 'Lebanon',
  },
  {
    code: 'KNA',
    name: 'Saint Kitts and Nevis',
  },
  {
    code: 'KWT',
    name: 'Kuwait',
  },
  {
    code: 'SGS',
    name: 'South Georgia',
  },
  {
    code: 'LUX',
    name: 'Luxembourg',
  },
  {
    code: 'MMR',
    name: 'Myanmar',
  },
  {
    code: 'ISR',
    name: 'Israel',
  },
  {
    code: 'TKM',
    name: 'Turkmenistan',
  },
  {
    code: 'GRC',
    name: 'Greece',
  },
  {
    code: 'MCO',
    name: 'Monaco',
  },
  {
    code: 'VEN',
    name: 'Venezuela',
  },
  {
    code: 'COG',
    name: 'Republic of the Congo',
  },
  {
    code: 'ESP',
    name: 'Spain',
  },
  {
    code: 'NFK',
    name: 'Norfolk Island',
  },
  {
    code: 'DMA',
    name: 'Dominica',
  },
  {
    code: 'TUN',
    name: 'Tunisia',
  },
  {
    code: 'TUR',
    name: 'Turkey',
  },
  {
    code: 'COL',
    name: 'Colombia',
  },
  {
    code: 'COM',
    name: 'Comoros',
  },
  {
    code: 'SLB',
    name: 'Solomon Islands',
  },
  {
    code: 'BLM',
    name: 'Saint Barthélemy',
  },
  {
    code: 'UZB',
    name: 'Uzbekistan',
  },
  {
    code: 'PSE',
    name: 'Palestine',
  },
  {
    code: 'ATA',
    name: 'Antarctica',
  },
  {
    code: 'CUB',
    name: 'Cuba',
  },
  {
    code: 'GAB',
    name: 'Gabon',
  },
  {
    code: 'KGZ',
    name: 'Kyrgyzstan',
  },
  {
    code: 'FSM',
    name: 'Micronesia',
  },
  {
    code: 'SWE',
    name: 'Sweden',
  },
  {
    code: 'MAF',
    name: 'Saint Martin',
  },
  {
    code: 'SVK',
    name: 'Slovakia',
  },
  {
    code: 'COD',
    name: 'DR Congo',
  },
  {
    code: 'MNE',
    name: 'Montenegro',
  },
  {
    code: 'GIB',
    name: 'Gibraltar',
  },
  {
    code: 'SOM',
    name: 'Somalia',
  },
  {
    code: 'LKA',
    name: 'Sri Lanka',
  },
  {
    code: 'CYP',
    name: 'Cyprus',
  },
  {
    code: 'LAO',
    name: 'Laos',
  },
  {
    code: 'MAR',
    name: 'Morocco',
  },
  {
    code: 'TLS',
    name: 'Timor-Leste',
  },
  {
    code: 'YEM',
    name: 'Yemen',
  },
  {
    code: 'FLK',
    name: 'Falkland Islands',
  },
  {
    code: 'GNQ',
    name: 'Equatorial Guinea',
  },
  {
    code: 'DZA',
    name: 'Algeria',
  },
  {
    code: 'ZMB',
    name: 'Zambia',
  },
  {
    code: 'SRB',
    name: 'Serbia',
  },
  {
    code: 'AND',
    name: 'Andorra',
  },
  {
    code: 'STP',
    name: 'São Tomé and Príncipe',
  },
  {
    code: 'DJI',
    name: 'Djibouti',
  },
  {
    code: 'BGR',
    name: 'Bulgaria',
  },
  {
    code: 'VIR',
    name: 'United States Virgin Islands',
  },
  {
    code: 'ARG',
    name: 'Argentina',
  },
  {
    code: 'NGA',
    name: 'Nigeria',
  },
  {
    code: 'RWA',
    name: 'Rwanda',
  },
  {
    code: 'SHN',
    name: 'Saint Helena, Ascension and Tristan da Cunha',
  },
  {
    code: 'CAN',
    name: 'Canada',
  },
  {
    code: 'PRK',
    name: 'North Korea',
  },
  {
    code: 'ALA',
    name: 'Åland Islands',
  },
  {
    code: 'MKD',
    name: 'North Macedonia',
  },
  {
    code: 'NLD',
    name: 'Netherlands',
  },
  {
    code: 'NER',
    name: 'Niger',
  },
  {
    code: 'SGP',
    name: 'Singapore',
  },
  {
    code: 'TCD',
    name: 'Chad',
  },
  {
    code: 'CUW',
    name: 'Curaçao',
  },
  {
    code: 'NIU',
    name: 'Niue',
  },
  {
    code: 'GNB',
    name: 'Guinea-Bissau',
  },
  {
    code: 'NRU',
    name: 'Nauru',
  },
  {
    code: 'ZWE',
    name: 'Zimbabwe',
  },
  {
    code: 'CHN',
    name: 'China',
  },
  {
    code: 'CXR',
    name: 'Christmas Island',
  },
  {
    code: 'PRI',
    name: 'Puerto Rico',
  },
  {
    code: 'ETH',
    name: 'Ethiopia',
  },
  {
    code: 'GRD',
    name: 'Grenada',
  },
  {
    code: 'FIN',
    name: 'Finland',
  },
  {
    code: 'TCA',
    name: 'Turks and Caicos Islands',
  },
  {
    code: 'BDI',
    name: 'Burundi',
  },
  {
    code: 'GUM',
    name: 'Guam',
  },
  {
    code: 'JOR',
    name: 'Jordan',
  },
  {
    code: 'MLI',
    name: 'Mali',
  },
  {
    code: 'UKR',
    name: 'Ukraine',
  },
  {
    code: 'REU',
    name: 'Réunion',
  },
  {
    code: 'SJM',
    name: 'Svalbard and Jan Mayen',
  },
  {
    code: 'CCK',
    name: 'Cocos (Keeling) Islands',
  },
  {
    code: 'TZA',
    name: 'Tanzania',
  },
  {
    code: 'QAT',
    name: 'Qatar',
  },
  {
    code: 'BRA',
    name: 'Brazil',
  },
  {
    code: 'SDN',
    name: 'Sudan',
  },
  {
    code: 'ROU',
    name: 'Romania',
  },
  {
    code: 'AIA',
    name: 'Anguilla',
  },
  {
    code: 'IRQ',
    name: 'Iraq',
  },
  {
    code: 'BTN',
    name: 'Bhutan',
  },
  {
    code: 'HND',
    name: 'Honduras',
  },
  {
    code: 'NAM',
    name: 'Namibia',
  },
  {
    code: 'SVN',
    name: 'Slovenia',
  },
  {
    code: 'HTI',
    name: 'Haiti',
  },
  {
    code: 'BIH',
    name: 'Bosnia and Herzegovina',
  },
  {
    code: 'GRL',
    name: 'Greenland',
  },
  {
    code: 'JAM',
    name: 'Jamaica',
  },
  {
    code: 'MLT',
    name: 'Malta',
  },
  {
    code: 'BOL',
    name: 'Bolivia',
  },
];

export const getCountryName = (code) => {
  const country = codeList.find((item) => item.code.toLowerCase() === code.toLowerCase());
  return country?.name;
};
