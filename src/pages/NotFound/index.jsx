import Logo from '@static/images/not-found.png';
import classes from './style.module.scss';

const NotFound = () => (
  <div className={classes.contentWrapper}>
    <img className={classes.image} src={Logo} alt="Not Found" />
    <div className={classes.title}>Halaman Tidak Ditemukan</div>
  </div>
);

export default NotFound;
