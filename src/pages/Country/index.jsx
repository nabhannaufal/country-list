import PropTypes from 'prop-types';
import { useEffect, useCallback } from 'react';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useParams } from 'react-router-dom';

import { getCountryDetail } from '@containers/App/actions';
import { selectCountryDetail, selectCountryDetailError, selectCountryLoading } from '@containers/App/selectors';
import { selectTheme } from '@containers/Locale/selectors';

import history from '@utils/history';

import CountryDetail from '@components/CountryDetail';
import CountryError from '@components/CountryError';
import Loader from '@components/Loader';
import BackButton from '@components/BackButton';

import classes from './style.module.scss';

const Country = ({ countryDetail, countryLoading, countryDetailError, theme }) => {
  const dispatch = useDispatch();
  const { name } = useParams();

  const getValue = (obj, category) => {
    if (obj) {
      return Object.keys(obj)
        .map((item) => {
          if (category) {
            return obj[item][category];
          }
          return obj[item];
        })
        .join(', ');
    }
    return null;
  };

  const title = countryDetail?.name?.common;
  const flagUrl = countryDetail?.flags?.png;
  const nativeName = countryDetail?.name?.nativeName;
  const nativeNameCommon = getValue(nativeName, 'common');
  const population = countryDetail?.population;
  const region = countryDetail?.region;
  const subRegion = countryDetail?.subregion;
  const capital = countryDetail?.capital?.join(', ');
  const topLevelDomain = countryDetail?.tld?.join(', ');
  const currencies = countryDetail?.currencies || {};
  const mainCurrencies = Object.keys(currencies)[0];
  const language = getValue(countryDetail?.languages);
  const borderCountry = countryDetail?.borders || [];

  const getData = useCallback(() => {
    dispatch(getCountryDetail(name));
  }, [name, dispatch]);

  useEffect(() => {
    getData();
  }, [getData]);

  const onBack = () => {
    history.push('/');
  };

  const onClickCountry = (countryName) => {
    history.push(countryName);
  };

  if (countryLoading) {
    return <Loader isLoading />;
  }

  if (countryDetailError) {
    return <CountryError onTryAgain={() => dispatch(getCountryDetail(name))} />;
  }

  return (
    <div className={classes.wrapper}>
      <BackButton darkTheme={theme === 'dark'} onBack={onBack} />
      <CountryDetail
        flagUrl={flagUrl}
        title={title}
        nativeName={nativeNameCommon}
        population={population}
        region={region}
        subRegion={subRegion}
        capital={capital}
        tld={topLevelDomain}
        currencies={mainCurrencies}
        language={language}
        borderCountry={borderCountry}
        onClickCountry={onClickCountry}
      />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  countryDetail: selectCountryDetail,
  theme: selectTheme,
  countryLoading: selectCountryLoading,
  countryDetailError: selectCountryDetailError,
});

Country.propTypes = {
  countryDetail: PropTypes.object,
  countryLoading: PropTypes.bool,
  countryDetailError: PropTypes.any,
  theme: PropTypes.string,
};

export default connect(mapStateToProps)(Country);
