import PropTypes from 'prop-types';
import { isEmpty, toLower } from 'lodash';
import { useEffect, useState, useMemo, useCallback } from 'react';
import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { getCountryList } from '@containers/App/actions';
import { selectCountryError, selectCountryList, selectCountryLoading } from '@containers/App/selectors';
import { selectTheme } from '@containers/Locale/selectors';

import history from '@utils/history';

import SearchBar from '@components/SearchBar';
import CardList from '@components/CardList';
import CountryError from '@components/CountryError';

import classes from './style.module.scss';

const Home = ({ countryList, countryLoading, countryError, theme }) => {
  const dispatch = useDispatch();
  const [keyword, setKeyword] = useState('');
  const [selectedRegion, setSelectedRegion] = useState('');

  const filterData = useMemo(() => {
    if (isEmpty(keyword)) return countryList;
    return countryList.filter((item) => toLower(item.name.common).includes(toLower(keyword)));
  }, [keyword, countryList]);

  const getData = useCallback(() => {
    dispatch(getCountryList(selectedRegion));
  }, [dispatch, selectedRegion]);

  useEffect(() => {
    getData();
  }, [getData]);

  const onClickCard = (name) => {
    history.push(`/${name}`);
  };

  if (countryError) {
    return <CountryError onTryAgain={getData} />;
  }

  return (
    <div className={classes.wrapper}>
      <SearchBar
        region={selectedRegion}
        darkTheme={theme === 'dark'}
        onSearch={(e) => setKeyword(e.target.value)}
        onSelect={(e) => setSelectedRegion(e.target.value)}
      />
      <CardList loading={countryLoading} data={filterData} onClickCard={onClickCard} />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  countryList: selectCountryList,
  countryLoading: selectCountryLoading,
  countryError: selectCountryError,
  theme: selectTheme,
});

Home.propTypes = {
  countryList: PropTypes.array,
  countryLoading: PropTypes.bool,
  countryError: PropTypes.any,
  theme: PropTypes.string,
};

export default connect(mapStateToProps)(Home);
