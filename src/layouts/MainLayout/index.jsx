import PropTypes from 'prop-types';

import { connect, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectTheme } from '@containers/Locale/selectors';
import { setTheme } from '@containers/Locale/actions';

import Navbar from '@components/Navbar';
import classes from './style.module.scss';

const propTypes = {
  children: PropTypes.element.isRequired,
  theme: PropTypes.string,
};

const MainLayout = ({ children, theme }) => {
  const dispatch = useDispatch();

  const onSwitch = () => {
    if (theme === 'dark') {
      dispatch(setTheme('light'));
    } else {
      dispatch(setTheme('dark'));
    }
  };
  return (
    <div className={classes.layoutWrapper}>
      <Navbar theme={theme} onSwitch={onSwitch} />
      {children}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

MainLayout.propTypes = propTypes;

export default connect(mapStateToProps)(MainLayout);
