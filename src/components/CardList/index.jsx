import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { Skeleton } from '@mui/material';

import en from '@wording/en';

import classes from './style.module.scss';

const CardList = ({ loading, data, onClickCard }) => {
  if (loading) {
    return (
      <div className={classes.loading}>
        {[...Array(10).keys()].map((index) => (
          <Skeleton className={classes.loadingCard} variant="rectangular" key={index} />
        ))}
      </div>
    );
  }
  if (isEmpty(data)) {
    return <div className={classes.notFound}>{en.notFound}</div>;
  }
  return (
    <div className={classes.contentWrapper}>
      {data.map((item, index) => (
        <div className={classes.card} key={index} onClick={() => onClickCard(item?.name.common)}>
          <img className={classes.flag} src={item?.flags.png} alt={item?.name} />
          <div className={classes.info}>
            <div className={classes.country}>{item?.name.common}</div>
            <div className={classes.infoTitle}>
              {en.population}
              <span>{item?.population}</span>
            </div>
            <div className={classes.infoTitle}>
              {en.region}
              <span>{item?.region}</span>
            </div>
            <div className={classes.infoTitle}>
              {en.capital}
              <span>{item?.capital}</span>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

CardList.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.array,
  onClickCard: PropTypes.func,
};

export default CardList;
