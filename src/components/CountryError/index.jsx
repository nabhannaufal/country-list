import PropTypes from 'prop-types';

import en from '@wording/en';

import classes from './style.module.scss';

const CountryError = ({ onTryAgain }) => (
  <div className={classes.error}>
    <div>{en.error}</div>
    <div className={classes.tryAgain} onClick={onTryAgain}>
      {en.tryAgain}
    </div>
  </div>
);

CountryError.propTypes = {
  onTryAgain: PropTypes.func,
};

export default CountryError;
