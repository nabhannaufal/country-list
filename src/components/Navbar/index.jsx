import PropTypes from 'prop-types';

import AppBar from '@mui/material/AppBar';

import en from '@wording/en';
import darkIcon from '@static/images/dark-icon.svg';
import lightIcon from '@static/images/light-icon.svg';

import classes from './style.module.scss';

const Navbar = ({ theme, onSwitch }) => {
  const isDark = theme === 'dark';
  return (
    <AppBar className={classes.headerWrapper}>
      <div className={classes.contentWrapper}>
        <div className={classes.title}>{en.navTitle}</div>
        <div className={classes.switcher} onClick={onSwitch}>
          <img className={classes.icon} src={isDark ? darkIcon : lightIcon} alt="" />
          <div className={classes.text}>{isDark ? en.lightMode : en.darkMode}</div>
        </div>
      </div>
    </AppBar>
  );
};

Navbar.propTypes = {
  theme: PropTypes.string,
  onSwitch: PropTypes.func,
};

export default Navbar;
