import PropTypes from 'prop-types';
import { Select, MenuItem } from '@mui/material';

import searhIconDark from '@static/images/search-icon-dark.svg';
import searhIconLight from '@static/images/search-icon-light.svg';

import en from '@wording/en';

import classes from './style.module.scss';

const SearchBar = ({ darkTheme, onSearch, onSelect, region }) => (
  <div className={classes.nav}>
    <div className={classes.searchWrapper}>
      <img src={darkTheme ? searhIconDark : searhIconLight} className={classes.searchIcon} alt="icon" />
      <input className={classes.input} placeholder={en.searchInputPlaceHolder} onChange={onSearch} />
    </div>
    <Select
      className={classes.filterRegion}
      value={region}
      displayEmpty
      onChange={onSelect}
      sx={{
        '& .MuiOutlinedInput-notchedOutline': {
          border: 'none',
        },
        '& .MuiSvgIcon-root': {
          color: darkTheme ? 'hsl(0, 0%, 100%)' : 'hsl(200, 15%, 8%)',
        },
      }}
    >
      <MenuItem value="" className={classes.menuItem}>
        {en.filterByRegion}
      </MenuItem>
      {en.regionList.map((item, index) => (
        <MenuItem value={item} key={index} className={classes.menuItem}>
          {item}
        </MenuItem>
      ))}
    </Select>
  </div>
);

SearchBar.propTypes = {
  darkTheme: PropTypes.bool,
  onSearch: PropTypes.func,
  onSelect: PropTypes.func,
  region: PropTypes.string,
};

export default SearchBar;
