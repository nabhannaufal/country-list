import PropTypes from 'prop-types';

import en from '@wording/en';

import backIconDark from '@static/images/back-icon-dark.svg';
import backIconLight from '@static/images/back-icon-light.svg';

import classes from './style.module.scss';

const BackButton = ({ darkTheme, onBack }) => (
  <div className={classes.backButton} onClick={onBack}>
    <img className={classes.icon} src={darkTheme ? backIconDark : backIconLight} alt="" />
    {en.back}
  </div>
);

BackButton.propTypes = {
  darkTheme: PropTypes.bool,
  onBack: PropTypes.func,
};

export default BackButton;
