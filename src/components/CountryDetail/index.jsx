import PropTypes from 'prop-types';
import { getCountryName } from '@utils/countryHelper';

import en from '@wording/en';

import classes from './style.module.scss';

const CountryDetail = ({
  flagUrl,
  title,
  nativeName,
  population,
  region,
  subRegion,
  capital,
  tld,
  currencies,
  language,
  borderCountry,
  onClickCountry,
}) => (
  <div className={classes.content}>
    <img className={classes.flag} src={flagUrl} alt={title} />
    <div className={classes.info}>
      <div className={classes.title}>{title}</div>
      <div className={classes.infoWrapper}>
        <div className={classes.infoLeft}>
          <div className={classes.infoContent}>
            <div className={classes.infoText}>
              {en.nativeName}
              <span>{nativeName}</span>
            </div>
            <div className={classes.infoText}>
              {en.population}
              <span>{population}</span>
            </div>
            <div className={classes.infoText}>
              {en.region}
              <span>{region}</span>
            </div>
            <div className={classes.infoText}>
              {en.subRegion}
              <span>{subRegion}</span>
            </div>
            <div className={classes.infoText}>
              {en.capital}
              <span>{capital}</span>
            </div>
          </div>
        </div>
        <div className={classes.infoRight}>
          <div className={classes.infoContent}>
            <div className={classes.infoText}>
              {en.topLevelDomain}
              <span>{tld}</span>
            </div>
            <div className={classes.infoText}>
              {en.currencies}
              <span>{currencies}</span>
            </div>
            <div className={classes.infoText}>
              {en.languages}
              <span>{language}</span>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.infoBottom}>
        <div>{en.borderCountries}</div>
        <div className={classes.borderCountry}>
          {borderCountry?.map((item, index) => {
            const countryName = getCountryName(item);
            return (
              <div className={classes.country} key={index} onClick={() => onClickCountry(countryName)}>
                {countryName}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  </div>
);

CountryDetail.propTypes = {
  flagUrl: PropTypes.string,
  title: PropTypes.string,
  nativeName: PropTypes.string,
  population: PropTypes.number,
  region: PropTypes.string,
  subRegion: PropTypes.string,
  capital: PropTypes.string,
  tld: PropTypes.string,
  currencies: PropTypes.string,
  language: PropTypes.string,
  borderCountry: PropTypes.array,
  onClickCountry: PropTypes.func,
};

export default CountryDetail;
