import { Route, Routes } from 'react-router-dom';

import routes from '@routes/index';

const ClientRoutes = () => {
  const getRoutes = () => {
    const routeList = [];

    routes.forEach((route) => {
      const { layout: Layout } = route;

      const renderElement = ({ component: Component }) => {
        if (typeof Layout !== 'undefined') {
          return (
            <Layout>
              <Component />
            </Layout>
          );
        }
        return <Component />;
      };

      if (route.subRoutes && route.subRoutes.length > 0) {
        route.subRoutes.forEach((subRoute) => {
          routeList.push({
            path: `${route.path}${subRoute.path}`,
            element: renderElement(subRoute),
          });
        });
      } else {
        routeList.push({
          path: route.path,
          element: renderElement(route),
        });
      }
    });

    return routeList;
  };

  return (
    <Routes>
      {getRoutes().map((route, i) => (
        <Route key={i} path={route.path} element={route.element} />
      ))}
    </Routes>
  );
};

export default ClientRoutes;
