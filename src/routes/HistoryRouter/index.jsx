import { useLayoutEffect, useState } from 'react';
import { Router } from 'react-router-dom';
import PropTypes from 'prop-types';

const propTypes = {
  basename: PropTypes.string,
  children: PropTypes.any,
  history: PropTypes.any,
};

const HistoryRouter = ({ basename, history, children }) => {
  const [state, setState] = useState({
    action: history.action,
    location: history.location,
  });

  useLayoutEffect(() => history.listen(setState), [history]);

  return (
    <Router navigator={history} location={state.location} navigationType={state.action} basename={basename}>
      {children}
    </Router>
  );
};

HistoryRouter.propTypes = propTypes;

export default HistoryRouter;
